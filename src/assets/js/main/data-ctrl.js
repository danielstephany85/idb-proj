function hasValue(item, value){
    var i;
     for(i = 0; i <  item.length; i++){
         if(item[i] == value){
             return true;
         }
     }
     return false;
}


(function () {
    'use strict';

    //check for support
    if (!('indexedDB' in window)) {
        console.log('This browser doesn\'t support IndexedDB');
        return;
    }
    var db; 

    function openDb(){
        var req = window.indexedDB.open('people-db', 1);

        req.onupgradeneeded = function (e) {
            console.log(hasValue(e.target.result.objectStoreNames, 'person'));
      
        if(!hasValue(e.target.result.objectStoreNames, 'person')){
            var peopleOS = e.currentTarget.result.createObjectStore('person', { keyPath: 'id', autoIncrement: true });
            peopleOS.createIndex('email', 'email', { unique: true });
            peopleOS.createIndex('gender', 'gender', { unique: false });
            peopleOS.createIndex('name', 'name', { unique: false });

        }
                
        };

        req.onerror = function (e) {
            console.log(e);
        };
        req.onsuccess = function (e) {
            db = e.target.result;
            getData();
        };
    }
    openDb();

       
    addData();
    function addData(){
        var personButton = document.querySelector("#addPerson");

        personButton.addEventListener('click', function (e) {
            var name = document.querySelector('#name');
            var email = document.querySelector('#email');
            var gender = document.querySelector('#gender');
            var tx = db.transaction('person', 'readwrite');
            var people = tx.objectStore('person');

            var item = {
                name: name.value,
                gender: gender.value,
                email: email.value,
                created: new Date().toLocaleString('en-US')
            };
            people.add(item);
            tx.onerror = function (e) {
                console.log(e);
                if (e.srcElement.source.keyPath == 'email') {
                    console.log('email is already taken');
                }
            }

            console.log('person added');

            return tx.complete;
        });
    }
    
    function getData(){
        var tx = db.transaction('person', 'readwrite');
        var people = tx.objectStore('person');
        people.openCursor().onsuccess = function (e) {
            var cursor = e.target.result;
            if (cursor) {
                console.log(cursor.value);
                cursor.continue();
            }
        };
    }


    function updateData() {
        var email = document.querySelector('#email2').value;
        var newEmail = document.querySelector('#newEmail').value;

        var tx = db.transaction('person', 'readwrite');
        var people = tx.objectStore('person');
        var emailstore = people.index('email');
        var req = emailstore.get(email);

        req.onsuccess = function (e) {
            var data = e.target.result;
            data.email = newEmail;
            console.log(data);

            // Put this updated object back into the database.
            var requestUpdate = people.put(data);
            requestUpdate.onerror = function (e) {
                console.log(e);
            };
            requestUpdate.onsuccess = function (event) {
                console.log('person updated');
            };
           
        };
    }

    updatePersonHandler()
    function updatePersonHandler(){
        var updateButton = document.querySelector('#updatePerson');

        updateButton.addEventListener('click', function(){
            console.log('click');
            updateData();
        });
    }
    
    getPerson();
    function getPerson(){
        document.querySelector('#getPerson').addEventListener('click', function(){
            var people = db.transaction('person', 'readwrite').objectStore('person');
            var name = document.querySelector('#name3').value;
            var nameIndex = people.index('name');
            var req = nameIndex.get(name);

            req.onsuccess = function(e){
                var data = e.target.result;
                console.log(data);
            }
            req.onerror = function(){

            }

        });
    }

})();